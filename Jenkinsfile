pipeline{
    agent { label 'master' }
    options{
        gitLabConnection('gitlab')
	skipDefaultCheckout true
	}
    parameters{
        gitParameter branchFilter: 'origin/(.*)', defaultValue: 'remote_deployment_ansible', name: 'GITBRANCH', type: 'PT_BRANCH'
    }
    stages{
        stage("Checkout Branch wise"){
            steps{
                git branch: "${params.GITBRANCH}", url: 'https://gitlab.com/jaydeepuniverse/deploy-petclinic.git', credentialsId: 'gitlab_ansible'
            }
        }
        stage("Deployment"){
            steps{
                ansiColor('xterm') {
                    ansiblePlaybook(
                        playbook: 'playbook.yaml',
                        inventory: 'hosts.ini',
                        colorized: true,
                    )
                }
            }
        }
	}
	post{
        failure{
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }
        success{
            updateGitlabCommitStatus name: 'build', state: 'success'
        }
		always{
			script{
				try {
					if (currentBuild.result == null) {
						currentBuild.result = 'SUCCESS'
					}
				} catch (err) {
					if (currentBuild.result == null) {
						currentBuild.result = 'FAILURE'
					}
					throw err
				} finally {
					influxDbPublisher selectedTarget: 'influxdb-monitoring'
				}
			}
		}
    }
}
